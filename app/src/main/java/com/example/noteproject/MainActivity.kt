package com.example.noteproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.noteproject.notes.NotesAdapter
import com.example.noteproject.utils.DaggerMyComponent
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val myComponent=DaggerMyComponent.create()
        //myComponent.inject(this)
    }
    companion object{
        var editingTheme:String =""
        var editingContent:String=""
        var addingNewNote=false
    }
}
