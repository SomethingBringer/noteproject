package com.example.noteproject.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NotesDatabaseDao {

    @Insert
    fun insert(note: Note)

    @Update
    fun update(note: Note)

    @Query("Select * from notes_table where theme=:theme")
    fun get(theme:String):Note

    @Query("Delete from notes_table where theme=:theme")
    fun delete(theme: String)

    @Query("Delete from notes_table")
    fun clear()

    @Query("Select * from notes_table")
    fun getAllNotes():LiveData<List<Note>>

}