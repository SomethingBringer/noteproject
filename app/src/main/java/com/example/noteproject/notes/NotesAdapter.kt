package com.example.noteproject.notes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.noteproject.database.Note
import com.example.noteproject.R

class NotesAdapter(onNoteListener: ViewHolder.NoteListener): RecyclerView.Adapter<NotesAdapter.ViewHolder>() {
    class ViewHolder(itemView: View, onNoteListener: NoteListener):RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var theme:TextView = itemView.findViewById(R.id.txtTheme)
        var content:TextView = itemView.findViewById(R.id.txtContent)
        var listener = onNoteListener
         init {
             itemView.setOnClickListener(this)
         }
        interface NoteListener{
            fun onNoteClick(position: Int)
        }

        override fun onClick(v: View?) {
            listener.onNoteClick(adapterPosition)
        }
    }
    var data= listOf<Note>()
    var noteListener = onNoteListener
    fun setDat(newData:List<Note>){
        this.data=newData
        notifyDataSetChanged()
    }
    fun getDat():List<Note>{
        return data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.note_item,parent,false)
        return ViewHolder(view, noteListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.theme.text=item.theme
        holder.content.text=item.content
    }
}