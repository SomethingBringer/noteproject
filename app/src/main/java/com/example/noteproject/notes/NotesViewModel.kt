package com.example.noteproject.notes

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.noteproject.database.Note
import com.example.noteproject.database.NotesDatabase
import com.example.noteproject.database.NotesDatabaseDao
import kotlinx.android.synthetic.main.note_item.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import javax.inject.Inject

class NotesViewModel(application: Application): AndroidViewModel(application) {

//    private var viewModelJob= Job()
//    override fun onCleared() {
//        super.onCleared()
//        viewModelJob.cancel()
//    }
//
//    private val uiScope= CoroutineScope(Dispatchers.Main+viewModelJob)
    val database= NotesDatabase.getInstance(application).notesDatabaseDao
    var notes = database.getAllNotes()

    private val _eventAddNote = MutableLiveData<Boolean>()
    val eventAddNote: LiveData<Boolean>
    get() = _eventAddNote

    private val _eventEditNote = MutableLiveData<Boolean>()
    val eventEditNote:LiveData<Boolean>
    get() = _eventEditNote

    private val _eventNavigate=MutableLiveData<Boolean>()
    val eventNavigate:LiveData<Boolean>
        get()=_eventNavigate
    fun addNewNote(){
        _eventAddNote.value=true
        _eventNavigate.value=true
    }
    fun editNote(){
        _eventEditNote.value=true
        _eventNavigate.value=true
    }
    init{
        _eventNavigate.value=false
        _eventEditNote.value=false
        _eventAddNote.value=false
    }
}