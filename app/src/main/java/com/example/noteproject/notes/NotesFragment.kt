package com.example.noteproject.notes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import com.example.noteproject.MainActivity.Companion.addingNewNote
import com.example.noteproject.MainActivity.Companion.editingContent
import com.example.noteproject.MainActivity.Companion.editingTheme
import com.example.noteproject.R
import com.example.noteproject.database.Note
import com.example.noteproject.database.NotesDatabase
import com.example.noteproject.databinding.FragmentNotesBinding
import com.example.noteproject.utils.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_notes.*
import javax.inject.Inject

class NotesFragment: Fragment(),LifecycleObserver,NotesAdapter.ViewHolder.NoteListener {
    //@Inject
    //lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: NotesViewModel
    lateinit var dat: List<Note>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding:FragmentNotesBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_notes, container, false)

        //val database = NotesDatabase.getInstance(application).notesDatabaseDao
        val application = requireNotNull(this.activity).application
        val adapter = NotesAdapter(this)
        binding.notesList.adapter=adapter
        val viewModelFactory = NotesViewModelFactory(application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(NotesViewModel::class.java)
       // dat = datSet(viewModel.notes)
        viewModel.notes.observe(this, Observer {notes ->
            adapter.setDat(notes)
            datSet(notes)
        })
        viewModel.eventNavigate.observe(this, Observer { navigating ->
            if(navigating){
                findNavController().navigate(R.id.action_notesFragment_to_editNoteFragment)
            }
        })
        binding.floatingActionButton2.setOnClickListener {
            viewModel.addNewNote()
            editingContent=""
            editingTheme=""
            addingNewNote=true
        }
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onNoteClick(position: Int) {
        val note = dat.get(position)
        editingTheme=note.theme
        editingContent=note.content
        viewModel.editNote()
    }
    fun datSet(newData:List<Note>){
        this.dat=newData
    }
}