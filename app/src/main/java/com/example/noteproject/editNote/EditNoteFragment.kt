package com.example.noteproject.editNote

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import com.example.noteproject.MainActivity.Companion.addingNewNote
import com.example.noteproject.MainActivity.Companion.editingContent
import com.example.noteproject.MainActivity.Companion.editingTheme
import com.example.noteproject.R
import com.example.noteproject.database.NotesDatabase
import com.example.noteproject.databinding.FragmentEditNoteBinding
import com.example.noteproject.utils.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_edit_note.*
import org.w3c.dom.Text
import javax.inject.Inject

class EditNoteFragment: Fragment() {
    //@Inject
    //lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: EditNoteViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentEditNoteBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_edit_note, container, false
        )
        if(editingTheme!="") binding.editTheme.setText(editingTheme)
        if (editingContent!="")binding.editContent.setText(editingContent)
        if (addingNewNote) binding.deleteBtn.visibility = View.GONE
            else binding.deleteBtn.visibility=View.VISIBLE
        val application = requireNotNull(this.activity).application
        val viewModelFactory = EditNoteViewModelFactory(application)
        val database = NotesDatabase.getInstance(application).notesDatabaseDao
        viewModel = ViewModelProvider(this,viewModelFactory).get(EditNoteViewModel::class.java)
        viewModel.eventNavigate.observe(this, Observer { navigating ->
            if(navigating){
                findNavController().navigate(R.id.action_editNoteFragment_to_notesFragment)
            }
        })

        binding.deleteBtn.setOnClickListener{
            viewModel.deleteNote(editTheme.text.toString())
            editingContent=""
            editingTheme=""
        }
        binding.pasteBtn.setOnClickListener{
            //viewModel.checkEditingTheme(editTheme.text.toString())
            viewModel.pasteNote(editTheme.text.toString(),editContent.text.toString(),it)
            editingContent=""
            editingTheme=""
            addingNewNote=false
        }
        binding.setLifecycleOwner(this)
        return binding.root
    }
}