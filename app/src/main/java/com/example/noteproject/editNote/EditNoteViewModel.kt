package com.example.noteproject.editNote

import android.app.Application
import android.provider.SyncStateContract.Helpers.insert
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.noteproject.MainActivity.Companion.editingTheme
import com.example.noteproject.database.Note
import com.example.noteproject.database.NotesDatabase
import kotlinx.coroutines.*
import javax.inject.Inject

class EditNoteViewModel(application: Application): AndroidViewModel(application) {
    private var viewModelJob= Job()
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
    private val uiScope= CoroutineScope(Dispatchers.Main+viewModelJob)
    val database= NotesDatabase.getInstance(application).notesDatabaseDao
    var notes = database.getAllNotes()

    private val _eventPasteNote=MutableLiveData<Boolean>()
    val eventPasteNote:LiveData<Boolean>
    get() =  _eventPasteNote

    private val _eventDeleteNote=MutableLiveData<Boolean>()
    val eventDeleteNote:LiveData<Boolean>
    get() = _eventDeleteNote

    private val _eventNavigate=MutableLiveData<Boolean>()
    val eventNavigate:LiveData<Boolean>
    get()=_eventNavigate

    fun checkEditingTheme(theme: String){
        uiScope.launch {
            get(theme)
        }
        deleteforInsert(editingTheme)
    }
    fun pasteNote(theme: String, content: String, view: View){
        deleteforInsert(editingTheme)
        _eventPasteNote.value=true
        _eventNavigate.value=true
        uiScope.launch {
                val newNote = Note(theme = theme, content = content)
                insert(newNote)
        }
    }
    private suspend fun insert(note:Note){
        withContext(Dispatchers.IO){
            database.insert(note)
        }
    }
    fun deleteforInsert(theme:String){
        uiScope.launch {
            delete(theme)
        }
    }
    fun deleteNote(theme:String){
        _eventDeleteNote.value=true
        _eventNavigate.value=true
        uiScope.launch {
            delete(theme)
        }
    }
    private suspend fun delete(theme:String){
        withContext(Dispatchers.IO){
            database.delete(theme)
        }
    }
    private suspend fun get(theme:String){
        withContext(Dispatchers.IO){

        }
    }
}