package com.example.noteproject.editNote

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EditNoteViewModelFactory(private val application: Application):ViewModelProvider.Factory {
    @Suppress("unchecked cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(EditNoteViewModel::class.java)){
            return EditNoteViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown View Model")
    }

}