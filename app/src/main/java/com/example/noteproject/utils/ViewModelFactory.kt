package com.example.noteproject.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
    private val viewModelsMap: MutableMap<Class<out ViewModel>,
            Provider<ViewModel>>):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        val creator = viewModelsMap[modelClass] ?:
//                viewModelsMap.asIterable().firstOrNull{
//                    modelClass.isAssignableFrom(it.key)
//                }?.value ?: throw IllegalArgumentException("unknown model class $modelClass")
//        return try {
//            @Suppress("UNCHECKED_CAST")
//            creator.get() as T
//        } catch (e: Exception){
//            throw RuntimeException(e)
//        }
        val viewModelProvider = viewModelsMap[modelClass]
            ?: throw IllegalArgumentException("model class $modelClass not found")
        return viewModelProvider.get() as T

    }

}