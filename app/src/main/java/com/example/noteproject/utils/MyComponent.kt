package com.example.noteproject.utils

import com.example.noteproject.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelFactoryModule::class,ViewModelModule::class])
interface MyComponent{
    fun inject(mainActivity: MainActivity)
}