package com.example.noteproject.utils

import androidx.lifecycle.ViewModel
import com.example.noteproject.editNote.EditNoteViewModel
import com.example.noteproject.notes.NotesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(EditNoteViewModel::class)
    internal abstract  fun bindEditNoteViewModel(editNoteViewModel: EditNoteViewModel):ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(NotesViewModel::class)
    internal abstract fun bindNotesViewModel(notesViewModel: NotesViewModel):ViewModel
}